﻿using System.Collections.Generic;

namespace UploadTrace.Core
{
    /// <summary>
    /// Service端的上传类
    /// </summary>
    public interface IUploadWorker
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="json">来自客户端的原始字符串内容</param>
        /// <param name="extraInfo">在界面上相应Worker的页面上设置的额外信息， 比如lineID等等</param>
        /// <returns></returns>
        string Upload(string json, Dictionary<string, string> extraInfo);

        /// <summary>
        /// 每个Worker的名字应该相互独立， worker的Name就是界面上相关Tab的header
        /// </summary>
        string Name { get; set; }
    }
}

