﻿namespace UploadTrace.Core
{
    public enum EditType
    {
        /// <summary>
        /// 新增worker
        /// </summary>
        NewWorker,
        /// <summary>
        /// 删除worker
        /// </summary>
        DeleteWorker,
        /// <summary>
        /// 修改现有的某个worker的配置
        /// </summary>
        ConfigWorker,
        /// <summary>
        /// 获取现有的所有worker的信息
        /// </summary>
        QueryWorkerList,
        /// <summary>
        /// 修改上传服务器的地址, 在局域网中多对1上传时有用
        /// </summary>
        EditUploadServerAddress,
        /// <summary>
        /// Editor尝试探测Service是否已经安装
        /// </summary>
        DetectServiceInstall
    }

    public enum CreateWorkerFrom
    {
        /// <summary>
        /// 用Python代码构建上传worker
        /// </summary>
        Python,
        /// <summary>
        /// 用.NET代码实现<see cref="IUploadWorker"/>构建上传worker
        /// </summary>
        Dll
    }
}