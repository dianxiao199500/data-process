﻿namespace UploadTrace.Core.Model
{
    public class UploadMessage
    {
        /// <summary>
        /// 用来上传的目标worker的名称
        /// </summary>
        public string WorkerName { get; set; }

        /// <summary>
        /// 从客户端收到的原始内容
        /// </summary>
        public string Content { get; set; }


        /// <summary>
        /// 在局域网中多对1发送时需要指定, Upload Service和Client都在同一机器时不需要
        /// 用于显示和统计某台机器的上传情况
        /// </summary>
        public string ClientId { get; set; }
    }
}