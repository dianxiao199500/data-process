﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using InVision.Base;

namespace InVision.Communication
{
    public class DataProperty
    {
        public int DataOffset { get; set; } = 0;
        public int DataLen { get; set; } = 0;
    }

    public static class SocketTool
    {
        // 帧头 2起始位 1功能码 4数据长度 
        // 数据
        // 帧尾 2 crc 1停止位
        public static readonly int FrameControlHeadLen = (1 + 2 + 4);
        public static readonly int FrameControlTailLen = (2 + 1);
        public static readonly int FrameControlLen = FrameControlHeadLen + FrameControlTailLen;
        public static readonly byte FramePrefix1 = 0xFA;
        public static readonly byte FramePrefix2 = 0x12;
        public static readonly byte FrameSufix = 0xAB;

        /// <summary>
        /// 获取打包的长度
        /// </summary>
        /// <param name="lenIn"></param>
        /// <returns></returns>
        public static int GetPackLen(int lenIn)
        {
            return lenIn + FrameControlLen;
        }

        /// <summary>
        /// 通过协议加密数据
        /// </summary>
        /// <param name="inDatas"></param>
        /// <param name="outDatas"></param>
        /// <returns></returns>
        public static int Pack(byte[] inDatas, ref byte[] outDatas, ref string errMsg)
        {
            if(inDatas.Length <= 0)
            {
                errMsg = $"输入长度错误!len={inDatas.Length}";
                return IVStatus.ERR;
            }

            outDatas = new byte[FrameControlLen + inDatas.Length];
            outDatas[0] = FramePrefix1;
            outDatas[1] = FramePrefix2;
            outDatas[2] = 0x00; //TODO:功能码
            outDatas[3] = (byte)(inDatas.Length & 0xFF);
            outDatas[4] = (byte)((inDatas.Length >> 8) & 0xFF);
            outDatas[5] = (byte)((inDatas.Length >> 16) & 0xFF);
            outDatas[6] = (byte)((inDatas.Length >> 24) & 0xFF);
            inDatas.CopyTo(outDatas, FrameControlHeadLen);
            outDatas[FrameControlHeadLen + inDatas.Length] = 0x00; // TODO CRC
            outDatas[FrameControlHeadLen + inDatas.Length + 1] = 0x00; // TODO CRC
            outDatas[FrameControlHeadLen + inDatas.Length + 2] = FrameSufix;

            return IVStatus.OK;
        }

        /// <summary>
        /// 通过协议加密数据
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="outDatas"></param>
        /// <returns></returns>
        public static int Pack(string msg, ref byte[] outDatas, ref string errMsg)
        {
            return Pack(Encoding.Default.GetBytes(msg), ref outDatas, ref errMsg);
        }

        /// <summary>
        /// 通过协议加密数据
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="outMsg"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public static int Pack(string msg, ref string outMsg, ref string errMsg)
        {
            byte[] outDatas = null;
            int status = Pack(Encoding.Default.GetBytes(msg), ref outDatas, ref errMsg);
            outMsg = Encoding.Default.GetString(outDatas);
            return status;
        }

        /// <summary>
        /// 按照协议解析
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="dataList"></param>
        /// <param name="remainOffset"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public static int Cut(byte[] bytes, ref List<DataProperty> dataList, ref int remainOffset, ref string errMsg)
        {
            if(dataList == null)
            {
                dataList = new List<DataProperty>();
            }
            else
            {
                dataList.Clear();
            }

            int offset = 0;
            while (true)
            {
                if (offset == bytes.Length) //当偏移量等于数组长度时，还没找到结束帧，则跳出
                {
                    break;
                }

                if(bytes.Length - offset - FrameControlHeadLen <= 0) // 如果数组长度-偏移量-帧头<=0 则表示中间可能出现多个帧头，解析报错
                {
                    errMsg = $"bytes.Length - offset - FrameControlHeadLen <= 0,bytes.length:{bytes.Length},offset:{offset},FrameControlHeadLen{FrameControlHeadLen}";
                    return IVStatus.ERR;
                }
                
                if(bytes[offset] != FramePrefix1 || bytes[offset + 1] != FramePrefix2) // 如果开始的时候并不是帧头，那么则去寻找帧头
                {
                    bool isFindStart = false;
                    for(int i = offset; i < bytes.Length; i++)
                    {
                        //todo:如果i的值恰好在结尾，那么是否会丢失数据? 如果找到bytes[i]==FreamPrefix1则return，截掉前面的所有数据?
                        if(bytes[i] == FramePrefix1 && i < bytes.Length - 1 && bytes[i + 1] == FramePrefix2) // 找到新的帧头
                        {
                            isFindStart = true;
                            offset = i;
                            break;
                        }
                    }

                    if (!isFindStart) // 如果找不到帧头，那表示收到了无用帧，丢弃此帧
                    {
                        offset = bytes.Length;
                    }
                    continue;
                }

                //解析帧尾
                int dataLen = bytes[offset + 3] | (bytes[offset + 4] << 8) | (bytes[offset + 5] << 16) | (bytes[offset + 6] << 24);
                if(bytes.Length - offset - FrameControlLen < dataLen)
                {
                    break;
                }

                DataProperty dataProperty = new();
                dataProperty.DataLen = dataLen;
                dataProperty.DataOffset = offset + FrameControlHeadLen;
                dataList.Add(dataProperty);
                offset += dataLen + FrameControlLen;
            }

            if(dataList.Count > 1)
            {
                errMsg = $"Multiple frames detected: {dataList.Count}, data len={bytes.Length}";
            }

            remainOffset = offset;
            return IVStatus.OK;
        }

        public static IPAddress GetLocalIp()
        {
            ///获取本地的IP地址
            string AddressIP = string.Empty;
            foreach (IPAddress _IPAddress in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
            {
                if (_IPAddress.AddressFamily.ToString() == "InterNetwork")
                {
                    AddressIP = _IPAddress.ToString();
                }
            }
            return IPAddress.Parse(AddressIP);
        }
    }
}
