﻿using InVision.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InVision.Communication
{
    public class SocketServerController
    {
        private TcpListener _tcpListener;
        private Dictionary<IPEndPoint, RemoteClient> _clients = new Dictionary<IPEndPoint, RemoteClient>();


        public int MaximumConnection { get; set; } = 10;
        public bool IsListening { get; private set; }
        public Dictionary<string, IPAddress> WhiteList { get; set; } = new Dictionary<string, IPAddress>();
        public int ClientReadCount { get; set; } = 102400;


        public event Action<IPEndPoint> InvalidConnection;
        public event Action<IPEndPoint, byte[]> ReceiveByte;
        public event Action<IPEndPoint, string> ReceiveStr;
        public event Action<IPEndPoint, string> ClientConnectionLost;
        public event Action<IPEndPoint> ClientConnect;
        public event Action<string> ServerError;
        public event Action<Exception> ServerException;
        public event Action ServerStart;
        public event Action ServerStop;

        /// <summary>
        /// 是否使用协议控制
        /// </summary>
        public bool IsUseFrameControl { get; set; } = false;
        /// <summary>
        /// 客户端接收到的消息
        /// </summary>
        public Dictionary<IPEndPoint, List<byte>> ClientRecBytes { get; set; } = new Dictionary<IPEndPoint, List<byte>>();

        public SocketServerController(IPEndPoint iPEndPoint)
        {
            _tcpListener = new TcpListener(iPEndPoint);
        }

        public SocketServerController(int port)
        {
            _tcpListener = new TcpListener(new IPEndPoint(IPAddress.Parse("127.0.0.1"), port));
        }

        public void StartListen()
        {
            if (IsListening)
                return;
            try
            {
                _tcpListener.Start();
            }
            catch (Exception ex)
            {
                OnServerError(ex.Message);
                return;
            }
            IsListening = true;
            OnServerStart();
            new Thread(() => WaitConnect()) { IsBackground = true }.Start();
        }

        public void StopListen()
        {
            if (_clients != null)
            {
                foreach (var client in _clients)
                {
                    client.Value.Close();
                }
            }

            lock (this)
            {
                ClientRecBytes.Clear();
            }
            IsListening = false;
            _tcpListener.Stop();
            OnServerStop();
        }

        public void DisconnectOne(IPEndPoint iPEndPoint)
        {
            if (_clients.Keys.Any(c => c == iPEndPoint))
            {
                _clients[iPEndPoint].Close();
            }

            lock (this)
            {
                if (ClientRecBytes.ContainsKey(iPEndPoint))
                {
                    ClientRecBytes.Remove(iPEndPoint);
                }
            }
        }

        public void DisconnectAll()
        {
            foreach (var client in _clients)
            {
                client.Value.Close();
            }

            lock (this)
            {
                ClientRecBytes.Clear();
            }
        }

        private void WaitConnect()
        {
            TcpClient client = new();
            while (IsListening)
            {
                try
                {
                    client = _tcpListener.AcceptTcpClient();
                }
                catch (Exception ex)
                {
                    if (IsListening)
                        OnServerError(ex.Message);
                    break;
                }
                var clientIPEndPoint = (IPEndPoint)client.Client.RemoteEndPoint;
                if (WhiteList.Count == 0 && _clients.Count < MaximumConnection)
                {
                    DoClientConnect(client, clientIPEndPoint);
                    continue;
                }
                if (WhiteList.Values.Any(ipadress => ipadress.Equals(clientIPEndPoint.Address)) && _clients.Count < MaximumConnection)
                {
                    DoClientConnect(client, clientIPEndPoint);
                    continue;
                }
                client.Close();
                OnInvalidConnection(clientIPEndPoint);
            }
        }

        private void DoClientConnect(TcpClient client, IPEndPoint clientIPEndPoint)
        {
            RemoteClient remoteClient = new RemoteClient(client, ClientReadCount);
            remoteClient.ReceiveStr += OnReceiveStr;
            remoteClient.ReceiveByte += OnReceiveByte;
            remoteClient.ConnectionLost += (ipEndPoint, msg) => OnClientConnectionLost(ipEndPoint, msg);

            _clients.Add(clientIPEndPoint, remoteClient);
            if (!ClientRecBytes.ContainsKey(clientIPEndPoint))
            {
                ClientRecBytes.Add(clientIPEndPoint, new List<byte>());
            }
            OnClientConnect(clientIPEndPoint);
        }

        public int SentToClient(IPEndPoint iPEndPoint, string str)
        {
            if (iPEndPoint == null || str == null || str.Length == 0)
                return IVStatus.ERR;
            if (!_clients.TryGetValue(iPEndPoint, out RemoteClient client))
                return IVStatus.ERR;
            try
            {
                string outMsg = str;
                if (IsUseFrameControl)
                {
                    string errMsg = string.Empty;
                    int status = SocketTool.Pack(str, ref outMsg, ref errMsg);
                    if(status != IVStatus.OK)
                    {
                        ServerError?.Invoke($"加密数据时出错:IP={iPEndPoint.Address},Msg={errMsg}");
                        return IVStatus.ERR;
                    }
                }
                client.Sent(outMsg);
                return IVStatus.OK;
            }
            catch (Exception ex)
            {
                ServerException?.Invoke(ex);
                return IVStatus.ERR;
            }
        }

        public int SentToClient(IPEndPoint iPEndPoint, byte[] bytes)
        {
            if (iPEndPoint == null || bytes == null || bytes.Length == 0)
                return IVStatus.ERR;
            if (!_clients.TryGetValue(iPEndPoint, out RemoteClient client))
                return IVStatus.ERR;
            try
            {
                if (IsUseFrameControl)
                {
                    byte[] outDatas = null;
                    string errMsg = string.Empty;
                    int status = SocketTool.Pack(bytes, ref outDatas, ref errMsg);
                    if(status != IVStatus.OK)
                    {
                        ServerError?.Invoke($"加密数据时出错:IP={iPEndPoint.Address},Msg={errMsg}");
                        return status;
                    }
                    client.Sent(outDatas);
                }
                else
                {
                    client.Sent(bytes);
                }

                return IVStatus.OK;
            }
            catch (Exception ex)
            {
                ServerException?.Invoke(ex);
                return IVStatus.ERR;
            }
        }


        private void OnInvalidConnection(IPEndPoint iPEndPoint)
        {
            InvalidConnection?.Invoke(iPEndPoint);
        }

        private void OnReceiveStr(IPEndPoint iPEndPoint, string str)
        {
            ReceiveStr?.Invoke(iPEndPoint, str);
        }

        private void OnReceiveByte(IPEndPoint iPEndPoint, byte[] bytes)
        {
            if (IsUseFrameControl)
            {
                lock (this)
                {
                    if (!ClientRecBytes.ContainsKey(iPEndPoint))
                    {
                        List<byte> byteList = new();
                        byteList.AddRange(bytes);
                        ClientRecBytes.Add(iPEndPoint, byteList);
                    }
                    else
                    {
                        ClientRecBytes[iPEndPoint].AddRange(bytes);
                    }

                    List<DataProperty> dataProperties = new List<DataProperty>();
                    int remainOffset = 0;
                    string errMsg = string.Empty;
                    int status = SocketTool.Cut(ClientRecBytes[iPEndPoint].ToArray(), ref dataProperties, ref remainOffset, ref errMsg);
                    if (status != IVStatus.OK)
                    {
                        ServerError(errMsg);
                        return;
                    }

                    foreach (var it in dataProperties)
                    {
                        byte[] recBytes = new byte[it.DataLen];
                        ClientRecBytes[iPEndPoint].CopyTo(it.DataOffset, recBytes, 0, it.DataLen);
                        ReceiveByte?.Invoke(iPEndPoint, recBytes);
                    }

                    ClientRecBytes[iPEndPoint].RemoveRange(0, remainOffset);
                }
            }
            else
            {
                ReceiveByte?.Invoke(iPEndPoint, bytes);
            }
        }

        private void OnServerError(string msg)
        {
            StopListen();
            ServerError?.Invoke(msg);
        }

        private void OnClientConnect(IPEndPoint iPEndPoint)
        {
            ClientConnect?.Invoke(iPEndPoint);
        }

        private void OnClientConnectionLost(IPEndPoint ipAdress, string msg)
        {
            _clients.Remove(ipAdress);
            ClientConnectionLost?.Invoke(ipAdress, msg);
        }

        private void OnServerStart()
        {
            ServerStart?.Invoke();
        }

        private void OnServerStop()
        {
            ServerStop?.Invoke();
        }
    }

    public class RemoteClient
    {
        private TcpClient _tcpClient;
        private int _readCount;
        private bool _isConnected = true;
        private IPEndPoint _ipEndPoint;

        public event Action<IPEndPoint, string> ConnectionLost;
        public event Action<IPEndPoint, string> ReceiveStr;
        public event Action<IPEndPoint, byte[]> ReceiveByte;


        public RemoteClient(TcpClient client, int readCount)
        {
            _tcpClient = client;
            _readCount = readCount;
            _ipEndPoint = (IPEndPoint)client.Client.RemoteEndPoint;
            new Thread(() => Receive()) { IsBackground = true }.Start();
            new Thread(() => CheckSocketAlive(_tcpClient.Client)) { IsBackground = true }.Start();
        }

        private void CheckSocketAlive(Socket s)
        {
            while (_isConnected)
            {
                Thread.CurrentThread.Join(100);
                try
                {
                    if ((s.Poll(0, SelectMode.SelectRead) && (s.Available == 0)) || !s.Connected)
                        OnConnectionLost("TCP client shutdown.");
                }
                catch (Exception)
                {
                    OnConnectionLost("TCP client shutdown.");
                }
            }
        }

        public void Receive()
        {
            while (_isConnected)
            {
                if (!_isConnected) break;

                var buffer = new byte[_readCount];
                int byteCount;
                try
                {
                    byteCount = _tcpClient.Client.Receive(buffer);
                }
                catch (SocketException ex)
                {
                    OnConnectionLost(ex.Message);
                    break;
                }
                if (byteCount == 0)
                    break;
                if(byteCount < buffer.Length)
                {
                    byte[] desBuffer = new byte[byteCount]; 
                    Array.ConstrainedCopy(buffer, 0, desBuffer, 0, byteCount);
                    new Thread(() => OnReceiveByte(desBuffer)) { IsBackground = true }.Start();
                }
                else
                {
                    new Thread(() => OnReceiveByte(buffer)) { IsBackground = true }.Start();
                }
                new Thread(() => OnReceiveStr(Encoding.ASCII.GetString(buffer, 0, byteCount))) { IsBackground = true }.Start();
            }
        }

        public void Sent(string str)
        {
            try
            {
                _tcpClient.Client.Send(Encoding.ASCII.GetBytes(str));
            }
            catch (Exception ex)
            {
                OnConnectionLost(ex.Message);
            }
        }

        public void Sent(byte[] bytes)
        {
            try
            {
                _tcpClient.Client.Send(bytes);
            }
            catch (Exception ex)
            {
                OnConnectionLost(ex.Message);
            }
        }

        public void Close()
        {
            _tcpClient.Client?.Disconnect(false);
            _tcpClient.Close();
        }

        private static readonly object _lock = new object();
        private void OnConnectionLost(string msg)
        {
            lock (_lock)
            {
                if (!_isConnected)
                    return;
                _isConnected = false;
                Close();
                ConnectionLost?.Invoke(_ipEndPoint, msg);
            }
        }


        private void OnReceiveStr(string str)
        {
            ReceiveStr?.Invoke(_ipEndPoint, str);
        }

        private void OnReceiveByte(byte[] bytes)
        {
            ReceiveByte?.Invoke(_ipEndPoint, bytes);
        }


    }
}
