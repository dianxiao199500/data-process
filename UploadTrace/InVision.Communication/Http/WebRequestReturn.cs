﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace InVision.Communication.Http
{
    public class WebRequestReturn
    {
        public string ResponseContent { get; set; } = string.Empty;

        public HttpStatusCode ResponseStatus { get; set; }

        public bool RequestStatus { get; set; }

    }
}
