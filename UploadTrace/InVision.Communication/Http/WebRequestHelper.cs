﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using InVision.Log;

namespace InVision.Communication.Http
{
    public static class WebRequestHelper
    {
        /// <summary>
        /// Http发送数据
        /// </summary>
        /// <param name="requstURL"></param>
        /// <param name="jsonParameter"></param>
        /// <returns>返回WebRequestReturn数据</returns>
        public static WebRequestReturn PostRequest(string requstURL, string jsonParameter)
        {
            var request = (HttpWebRequest)WebRequest.Create(requstURL);
            request.ContentType = "application/json";
            request.Accept = "application/json";
            request.Method = "post";
            request.Timeout = 400;
            try
            {
                var _requestStream = request.GetRequestStream();
                var bytes = Encoding.UTF8.GetBytes(jsonParameter);
                _requestStream.Write(bytes, 0, bytes.Length);
                _requestStream.Flush();
                _requestStream.Close();
                var httpResponse = (HttpWebResponse)request.GetResponse();
                string responseContent = new StreamReader(httpResponse.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                $"上传{jsonParameter}到{requstURL}成功，StatusCode:{HttpStatusCode.OK}，回复内容:{responseContent}".LogInfo();
                return new WebRequestReturn() { RequestStatus = true, ResponseStatus = HttpStatusCode.OK, ResponseContent = responseContent };
            }
            catch (WebException e)
            {
                if (e.Response == null)
                {
                    $"上传{jsonParameter}到{requstURL}失败，{HttpStatusCode.RequestTimeout}".LogError();
                    return new WebRequestReturn() { RequestStatus = false, ResponseStatus = HttpStatusCode.RequestTimeout, ResponseContent = "Can not find service." };
                }
                HttpWebResponse response = (HttpWebResponse)e.Response;
                string responseContent = new StreamReader(response.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                $"上传{jsonParameter}到{requstURL}失败，StatusCode:{response.StatusCode}，回复内容:{responseContent}".LogError();
                return new WebRequestReturn() { RequestStatus = true, ResponseStatus = response.StatusCode, ResponseContent = responseContent };
            }
        }

        public static WebRequestReturn GetRequest(string requstURL, Dictionary<string, string> pairs)
        {
            requstURL += "?" + BuildQuery(pairs);
            var request = (HttpWebRequest)WebRequest.Create(requstURL);
            request.Accept = "application/json";
            request.Method = "get";
            request.Timeout = 400;
            try
            {
                var httpResponse = (HttpWebResponse)request.GetResponse();
                string responseContent = new StreamReader(httpResponse.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                $"收到Http回复，StatusCode:{HttpStatusCode.OK}，回复内容{responseContent}".LogInfo();
                return new WebRequestReturn() { RequestStatus = true, ResponseStatus = HttpStatusCode.OK, ResponseContent = responseContent };
            }
            catch (WebException e)
            {
                if (e.Response == null)
                {
                    $"没有从{requstURL}收到回复".LogError();
                    return new WebRequestReturn() { RequestStatus = false, ResponseStatus = HttpStatusCode.RequestTimeout, ResponseContent = "Can not find service." };
                }
                HttpWebResponse response = (HttpWebResponse)e.Response;
                string responseContent = new StreamReader(response.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                $"收到Http回复，StatusCode:{response.StatusCode}，回复内容{responseContent}".LogError();
                return new WebRequestReturn() { RequestStatus = true, ResponseStatus = response.StatusCode, ResponseContent = responseContent };
            }
        }

        public static WebRequestReturn GetRequest(string requstURL, Dictionary<string, string> pairs, string username, string password)
        {
            requstURL += "?" + BuildQuery(pairs);
            var request = (HttpWebRequest)WebRequest.Create(requstURL);
            request.Accept = "application/json";
            request.Method = "get";
            request.Timeout = 400;
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}")));
            try
            {
                var httpResponse = (HttpWebResponse)request.GetResponse();
                string responseContent = new StreamReader(httpResponse.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                $"收到Http回复，StatusCode:{HttpStatusCode.OK}，回复内容{responseContent}".LogInfo();
                return new WebRequestReturn() { RequestStatus = true, ResponseStatus = HttpStatusCode.OK, ResponseContent = responseContent };
            }
            catch (WebException e)
            {
                if (e.Response == null)
                {
                    $"没有从{requstURL}收到回复".LogError();
                    return new WebRequestReturn() { RequestStatus = false, ResponseStatus = HttpStatusCode.RequestTimeout, ResponseContent = "Can not find service." };
                }
                HttpWebResponse response = (HttpWebResponse)e.Response;
                string responseContent = new StreamReader(response.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                $"收到Http回复，StatusCode:{response.StatusCode}，回复内容{responseContent}".LogError();
                return new WebRequestReturn() { RequestStatus = true, ResponseStatus = response.StatusCode, ResponseContent = responseContent };
            }
        }

        /// <summary>
        /// 组装普通文本请求参数。
        /// </summary>
        /// <param name="parameters">Key-Value形式请求参数字典</param>
        /// <returns>URL编码后的请求数据</returns>
        public static string BuildQuery(IDictionary<string, string> parameters)
        {
            if (parameters == null || parameters.Count == 0)
            {
                return null;
            }

            StringBuilder query = new StringBuilder();
            bool hasParam = false;

            foreach (KeyValuePair<string, string> kv in parameters)
            {
                string name = kv.Key;
                string value = kv.Value;
                // 忽略参数名或参数值为空的参数
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
                {
                    if (hasParam)
                    {
                        query.Append("&");
                    }
                    query.Append(name);
                    query.Append("=");
                    query.Append(HttpUtility.UrlEncode(value, Encoding.UTF8));
                    hasParam = true;
                }
            }

            return query.ToString();
        }

    }
}
