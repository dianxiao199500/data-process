﻿using InVision.Base;
using InVision.Log;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InVision.Interpreter
{
    /// <summary>
    /// Python脚本解释器，注意，在第一次调用的时候，可能由于环境加载，会导致有一些缓慢
    /// </summary>
    public class PythonInterpreter : Interpreter
    {
        /// <summary>
        /// 设置python脚本的输入参数
        /// </summary>
        public Dictionary<string, object> InputParams { get; set; } = new Dictionary<string, object>();

        public PythonInterpreter()
        {
            Engine = Python.CreateEngine();
            Scope = Engine.CreateScope();
            ImportMoudules();
        }

        public PythonInterpreter(IDictionary<string, object> options)
        {
            Engine = Python.CreateEngine(options);
            Scope = Engine.CreateScope();
            ImportMoudules();
        }

        private void ImportMoudules()
        {
            new Thread(() =>
            {
                Engine.ImportModule("json");
            }).Start();
        }

        public override EScriptRunStatus Execute(string runCode, ref dynamic result)
        {
            try
            {
                foreach(var it in InputParams)
                {
                    Scope.SetVariable(it.Key, it.Value);
                }

                ScriptSource script = Engine.CreateScriptSourceFromString(runCode);
                result = script.Execute(Scope);
                //var add = Scope.GetVariable<Func<object, object>>("add");
                //Console.WriteLine(add(InputParam));
                if(result == null)
                {
                    "脚本执行成功，但是没有返回值!".LogWarn();
                    return EScriptRunStatus.SuccessButNotRet;
                }

                return EScriptRunStatus.OK;
            }
            catch (Exception ex)
            {
                "脚本执行异常!信息如下:".LogError();
                ex.Log();
                return EScriptRunStatus.Exception;
            }

        }

        public override EScriptRunStatus ExecuteByFile(string filePath, ref dynamic result)
        {
            try
            {
                if (!File.Exists(filePath))
                {
                    string msg = $"未找到文件:{filePath}";
                    msg.LogError();
                    return EScriptRunStatus.PathNotExist;
                }

                string contents = File.ReadAllText(filePath);

                return Execute(contents, ref result);
            }
            catch (Exception ex)
            {
                "脚本执行异常!信息如下:".LogError();
                ex.Log();
                return EScriptRunStatus.Exception;
            }   
        }
    }
}
