﻿using Microsoft.Scripting.Hosting;
using InVision.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InVision.Interpreter
{
    public abstract class Interpreter
    {
        /// <summary>
        /// 动态语言执行类，可于解析和执行动态语言代码
        /// </summary>
        protected ScriptEngine Engine { get; init; }

        /// <summary>
        /// 构建一个执行上下文，其中保存了环境及全局变量；宿主(Host)可以通过创建不同的 ScriptScope 来提供多个数据隔离的执行上下文
        /// </summary>
        protected ScriptScope Scope { get; init; }

        private string _runCode = string.Empty;
        /// <summary>
        /// 运行脚本,与运行路径<see cref="CodePath"/>二选一
        /// </summary>
        public string RunCodeStr
        {
            get => _runCode;
            set
            {
                if (_runCode.Equals(value))
                {
                    return;
                }

                _runCode = value;
                CodePath = string.Empty;
            }
        }

        private string _codePath = string.Empty;
        /// <summary>
        /// 脚本路径,与运行脚本<see cref="RunCodeStr"/>二选一
        /// </summary>
        public string CodePath
        {
            get => _codePath;
            set
            {
                if (_codePath.Equals(value))
                {
                    return;
                }

                _codePath = value;
                RunCodeStr = string.Empty;
            }
        }

        /// <summary>
        /// 执行脚本语言
        /// </summary>
        /// <param name="runCode">运行的代码</param>
        /// <returns></returns>
        public abstract EScriptRunStatus Execute(string runCode, ref dynamic result);

        /// <summary>
        /// 通过脚本文件执行脚本语言
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public abstract EScriptRunStatus ExecuteByFile(string filePath, ref dynamic result);

        /// <summary>
        /// 自选脚本或者脚本路径
        /// </summary>
        /// <returns></returns>
        public EScriptRunStatus Execute(ref dynamic result)
        {
            result = null;

            if (!RunCodeStr.IsNullOrEmpty())
            {
                return Execute(RunCodeStr, ref result);
            }

            if (!CodePath.IsNullOrEmpty())
            {
                return ExecuteByFile(CodePath, ref result);
            }

            return EScriptRunStatus.PathAndCodeIsNull;
        }
    }
}
