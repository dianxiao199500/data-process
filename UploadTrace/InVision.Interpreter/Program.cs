﻿using System;
using System.IO;
using System.Threading;
using InVision.Log;
using IronPython.Hosting;
using IronPython.Runtime;
using Microsoft.Scripting.Hosting;

namespace InVision.Interpreter
{
    class Program
    {
        static void Main(string[] args)
        {
            IVSyncLog.IsWriteToConsole = true;
            try
            {
                string msg = string.Empty;
                PythonInterpreter interpreter = new();
                //var result = interpreter.Execute(ref msg, $"import datetime{Environment.NewLine}print 'current time is:',datetime.datetime.now()");
                interpreter.InputParams.Add("json_str", "{\"key\":\"www\"}");
                Thread.Sleep(4000);
                //var result = interpreter.Execute($"def add(a):{Environment.NewLine}\treturn a + 1{Environment.NewLine}add(a)");
                //string contents = File.ReadAllText(@"F:\pyProject\test.py");
                for (int i = 0; i < 100; i++)
                {
                    DateTime startTime = DateTime.Now;
                    dynamic result = null;
                    var status = interpreter.ExecuteByFile(@"F:\pyProject\test.py", ref result);
                    DateTime endTime = DateTime.Now;
                    $"脚本执行完毕!Result={result ?? "null"},耗时:{(endTime - startTime).TotalMilliseconds}ms".LogInfo();
                }
            }
            catch (Exception ex)
            {
                ex.Log();
            }
            Console.ReadKey();
        }
    }
}
