﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InVision.Base
{
    public static class IVStatus
    {
        public static readonly int OK = 1;
        public static readonly int ERR = 0;
        public static readonly int TRUE = 1;
        public static readonly int FALSE = 0;
    }
}
