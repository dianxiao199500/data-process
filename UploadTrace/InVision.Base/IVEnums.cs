﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InVision.Base
{
    /// <summary>
    /// 上传方式
    /// </summary>
    [Serializable]
    public enum UploadMethod
    {
        Http = 0,
        Socket,
        SerialPort
    }

    /// <summary>
    /// 上传类型
    /// </summary>
    [Serializable]
    public enum UploadType
    {
        TimeInterval = 0, //固定时间间隔
        RealTime, //实时上传
    }

    public enum UploadHttpStatus
    {
        OK = 0,
        Error,
        UriError,
        NullUri,
        InvalidConfigPath,
        Exception
    }

    public enum GlobalCode
    {
        [Description("执行OK")]
        OK = 0,
        [Description("执行出错")]
        Error,
        [Description("执行出现异常")]
        Exception
    }

    /// <summary>
    /// 脚本运行状态
    /// </summary>
    public enum EScriptRunStatus
    {
        [Description("脚本运行OK")]
        OK = 0,
        [Description("脚本执行成功但没有返回值")]
        SuccessButNotRet,
        [Description("脚本执行异常")]
        Exception,
        [Description("脚本路径不存在")]
        PathNotExist,
        [Description("脚本与脚本路径均未设置")]
        PathAndCodeIsNull,
    }

}
