﻿using InVision.Log;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace InVision.Base
{
    public static class GlobalExpansions
    {
        /// <summary>
        /// 字符串是否为null或者空
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        /// <summary>
        /// 创建目录
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool CreateDir(this string str)
        {
            try
            {
                if (!Directory.Exists(str))
                {
                    Directory.CreateDirectory(str);
                }
                return true;
            }
            catch (Exception ex)
            {
                ex.Log();
                return false;
            }
        }

        /// <summary>
        /// 使得确保字符串以 "\" 结尾
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string FixedDir(this string str)
        {
            if (str.IsNullOrEmpty())
            {
                return str;
            }

            if (!str.EndsWith("\\"))
            {
                str += "\\";
            }
            return str;
        }

        /// <summary>
        /// 获取枚举的描述
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum val)
        {
            Type enumType = val.GetType();

            // 获取枚举常数名称。
            string name = Enum.GetName(enumType, val);
            if (name != null)
            {
                // 获取枚举字段。
                FieldInfo fieldInfo = enumType.GetField(name);
                if (fieldInfo != null)
                {
                    // 获取描述的属性。
                    DescriptionAttribute attr = Attribute.GetCustomAttribute(fieldInfo,
                        typeof(DescriptionAttribute), false) as DescriptionAttribute;
                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }
            }

            return string.Empty;
        }
    }
}
