﻿using NetMQ.Sockets;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UploadTrace.Core;
using UploadTrace.Core.Model;
using UploadTrace.Service.UploadWorkers;

namespace UploadTrace.Service
{
    public class TraceUploadService
    {
        private const string AppDataDir = "c:/ProgramData/TraceUploadService";
        private readonly Dictionary<string, IUploadWorker> _workerByName = new Dictionary<string, IUploadWorker>();
        private ResponseSocket _uploadSocket;
        private string _uploadServerAddress;



        public void RestartUploadListeningLoops()
        {
            void OnUploadMessageReceived(string message)
            {
                var messageObj = JsonConvert.DeserializeObject<UploadMessage>(message);


            }

            _uploadSocket?.Close();
            _uploadSocket?.Dispose();

            _uploadSocket = new ResponseSocket(_uploadServerAddress);


        }




        public TraceUploadService()
        {
            // Load configured workers
            var configDir = Path.Combine(AppDataDir, "Workers");
            Directory.CreateDirectory(configDir);
            var workerFiles = Directory.GetFiles(configDir, "*.worker");

            foreach (var workerFile in workerFiles)
            {
                var configText = File.ReadAllText(workerFile);
                var worker = CreateWorkerFromText(configText);
                _workerByName[worker.Name] = worker;
            }

            RestartUploadListeningLoops();

            // Start listening for editing requests
            void OnEditMessageReceived(string message)
            {
                var messageObj = JsonConvert.DeserializeObject<EditMessage>(message);
                switch (messageObj.EditType)
                {
                    case EditType.NewWorker:
                        var worker = CreateWorkerFromText(messageObj.Message);
                        _workerByName[worker.Name] = worker;
                        RestartUploadListeningLoops();
                        break;


                }
            }

        }


        private IUploadWorker CreateWorkerFromText(string message)
        {
            var textArray = message.Split(';');
            var workerType = Enum.Parse<CreateWorkerFrom>(textArray[0]);
            IUploadWorker worker = null;
            if (workerType == CreateWorkerFrom.Dll)
            {
                var dllPath = textArray[1];
                var fullName = textArray[2];
                var workerName = textArray[3];
                var assembly = Assembly.LoadFrom(dllPath);
                var targetType = assembly.GetTypes().Where(t => t.FullName == fullName).First();
                worker = (IUploadWorker)Activator.CreateInstance(targetType);
                worker.Name = workerName;
            }

            if (workerType == CreateWorkerFrom.Python)
            {
                var configFilePath = textArray[1];
                var workerName = textArray[2];
            }

            // Save worker config text
            var configDir = Path.Combine(AppDataDir, "Workers");
            Directory.CreateDirectory(configDir);
            var filePath = Path.Combine(configDir, $"{worker.Name}.worker");
            File.WriteAllText(filePath, message);

            return worker;
        }
    }
}
