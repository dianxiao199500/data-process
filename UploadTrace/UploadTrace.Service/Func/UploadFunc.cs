﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using InVision.Log;
using System.IO;
using InVision.Base;
using Newtonsoft.Json;
using System.Xml.Serialization;
using System.ComponentModel;
using InVision.Interpreter;

namespace UploadTrace.Service.Func
{
    [Serializable]
    [XmlInclude(typeof(UploadHttpStep))]
    [XmlInclude(typeof(RecordReportStep))]
    public abstract class BaseStep
    {
        /// <summary>
        /// 使用前面步骤的索引
        /// </summary>
        [Description("使用前面步骤的索引")]
        public int UsingBeforeStepIdx { get; set; } = -1;

        /// <summary>
        /// 当前步骤的索引
        /// </summary>
        [Description("当前步骤的索引")]
        public int StepIndex { get; set; } = 0;

        /// <summary>
        /// 上传所使用的方式
        /// </summary>
        [Description("上传所使用的方式, 0-http, 1-TCP, 2-SerialPort")]
        public UploadMethod UploadMethod { get; set; } = UploadMethod.Http;

        /// <summary>
        /// 上传方式
        /// </summary>
        [Description("上传方式, 0-固定时间间隔上传, 1-实时上传")]
        public UploadType UploadType { get; init; } = UploadType.RealTime;

        /// <summary>
        /// 间隔上传的时间(min)
        /// </summary>
        [Description("间隔上传的时间(min)")]
        public int Interval { get; init; } = 60;

        /// <summary>
        /// 编码
        /// </summary>
        protected Encoding encoding = Encoding.Default;
        public void SetEncoding(Encoding encode)
        {
            encoding = encode;
        }

        public Encoding GetEncoding()
        {
            return encoding;
        }

        public abstract GlobalCode Execute(ref string responseMsg, bool isSaveResult = false, string saveDir = "E:\\UploadTraceDemo\\");

        public abstract GlobalCode Init();

        public BaseStep()
        {

        }
    }

    /// <summary>
    /// 上传的步骤所需参数
    /// </summary>
    [Serializable]
    public class UploadHttpStep : BaseStep
    {
        /// <summary>
        /// 请求的Uri
        /// </summary>
        private Uri _uri;
        /// <summary>
        /// 上传的参数,默认使用JObject，注意，key表示上传字段的名称，value表示拿到的数据的字段名称
        /// </summary>
        private JObject _requestParams = new();
        /// <summary>
        /// key: 表示接收到的参数的key
        /// value: 表示_requestParams的key
        /// </summary>
        private JObject _requestFillKeys = new();
        /// <summary>
        /// 所需要从Response中拿到的字段的值
        /// </summary>
        private JObject _needParamsFromResponse = new();
        /// <summary>
        /// python脚本解释器
        /// </summary>
        private PythonInterpreter _interpreter = new PythonInterpreter();

        /// <summary>
        /// 字符串形式的Uri
        /// </summary>
        public string UriString { get; set; } = "http://d667ede4-38cb-45c7-a0a4-a9076b9e9495.mock.pstmn.io/hello";

        /// <summary>
        /// 步骤名称
        /// </summary>
        [Description("步骤名称")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// 步骤配置名称
        /// </summary>
        public string ConfigName { get; set; } = ".\\Config\\UploadConfig\\Step.json";

        /// <summary>
        /// 脚本内容或者脚本路径
        /// </summary>
        public string ScriptCodeOrPath { get; set; } = @"F:\pyProject\test.py";

        /// <summary>
        /// 请求方式
        /// </summary>
        [Description("请求方式 0-GET, 1-POST")]
        public Method RequestMethod { get; set; } = Method.POST;

        /// <summary>
        /// 上传方式
        /// </summary>
        [Description("上传方式")]
        public string ContentType { get; set; } = "application/json";

        /// <summary>
        /// 所接收的回复内容
        /// </summary>
        [Description("所接收的回复内容")]
        public string Accept { get; set; } = "*/*";

        /// <summary>
        /// 请求超时 (ms)
        /// </summary>
        [Description("请求超时(ms)")]
        public int Timeout { get; set; } = 2000;

        private Func<int, JObject> _getOtherStepResult = null; 

        public UploadHttpStep()
        {

        }

        public override GlobalCode Init()
        {
            SetScriptCodeOrPath(ScriptCodeOrPath);
            if (Uri.TryCreate(UriString, UriKind.RelativeOrAbsolute, out Uri result))
            {
                _uri = result;
            }
            else
            {
                $"输入Uri错误,input uri={UriString}".LogError();
                return GlobalCode.Error;
            }

            var status = LoadKeysFromConfig(ConfigName);
            if(status != UploadHttpStatus.OK)
            {
                return GlobalCode.Error;
            }

            return GlobalCode.OK;
        }

        /// <summary>
        /// 从配置文件加载字段的Key
        /// </summary>
        /// <param name="configName"></param>
        /// <returns></returns>
        private UploadHttpStatus LoadKeysFromConfig(string configName)
        {
            if (!File.Exists(configName))
            {
                $"未找到配置文件:{configName}".LogError();
                return UploadHttpStatus.InvalidConfigPath;
            }

            string contents = File.ReadAllText(configName);
            try
            {
                _requestFillKeys.RemoveAll();
                _requestFillKeys = JObject.Parse(contents);
            }
            catch (Exception ex)
            {
                $"转换为json失败!content={contents},configName={configName}".LogError();
                ex.Log();
                return UploadHttpStatus.Exception;
            }

            return UploadHttpStatus.OK;
        }

        public void RegisterGetOtherStepFunc(Func<int, JObject> func)
        {
            _getOtherStepResult = func;
        }

        /// <summary>
        /// 所需要从Response中拿到的字段的值
        /// </summary>
        public JObject GetNeedParamsFromResponse()
        {
            return _needParamsFromResponse;
        }

        /// <summary>
        /// 设置脚本或者脚本路径
        /// </summary>
        /// <param name="codeOrPath"></param>
        /// <returns></returns>
        public void SetScriptCodeOrPath(string codeOrPath)
        {
            if (File.Exists(codeOrPath))
            {
                _interpreter.CodePath = codeOrPath;
            }
            else
            {
                _interpreter.RunCodeStr = codeOrPath;
            }
        }

        /// <summary>
        /// 填充Json的Body
        /// </summary>
        /// <param name="pairs">当前拥有的所有字段</param>
        /// <param name="fillJobj">所填充的JObject</param>
        /// <param name="globalPairs">全局拥有的字段</param>
        private void FillRequest(JObject pairs, JObject existsPairs, Dictionary<string, string> globalPairs, ref JObject fillJobj)
        {
            if (existsPairs == null || existsPairs.Count <= 0)
            {
                "没有填充Body，因为:\"existsPairs == null || existsPairs.Count <= 0\",请检查是否已加载配置文件!".LogError();
                return;
            }

            //if(_requestFillKeys == null || _requestFillKeys.Count <= 0)
            //{
            //    "没有填充Body，因为:\"_requestFillKeys == null || _requestFillKeys.Count <= 0\",请检查是否已加载配置文件!".LogError();
            //    return;
            //}

            foreach (var it in existsPairs)
            {
                if (it.Value.Type == JTokenType.Object || it.Value.Type == JTokenType.Array)
                {
                    var strs = it.Key.Split('-');
                    if (strs.Length > 1)
                    {
                        if (pairs.ContainsKey(strs[1]))
                        {
                            $"检查到所需填充的Json中不需要key={strs[1]}，请检查配置是否demo配置正确!".LogWarn();
                            continue;
                        }
                        JObject jobj = new();
                        FillRequest(pairs[strs[1]].ToObject<JObject>(), it.Value.ToObject<JObject>(), globalPairs, ref jobj);
                        fillJobj[strs[0]] = jobj;
                    }
                    else
                    {
                        if (fillJobj.ContainsKey(it.Key))
                        {
                            $"检查到所需填充的Json中不需要key={it.Key}，请检查配置是否demo配置正确!".LogWarn();
                            continue;
                        }
                        JObject jobj = new();
                        FillRequest(pairs[it.Key].ToObject<JObject>(), it.Value.ToObject<JObject>(), globalPairs, ref jobj);
                        fillJobj[it.Key] = jobj;
                    }
                    continue;
                }

                if (pairs.ContainsKey(it.Value.ToString()))
                {
                    fillJobj[it.Key] = pairs[it.Value.ToString()];
                }
                else
                {
                    //在全局的pairs寻找值
                    if (globalPairs != null)
                    {
                        foreach (var iter in globalPairs)
                        {
                            if (fillJobj.ContainsKey(iter.Key))
                            {
                                fillJobj[iter.Key] = iter.Value;
                                break;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 填充Json的Body
        /// </summary>
        /// <param name="pairs">接收到的所有数据</param>
        /// <param name="globalPairs">全局的所有字段</param>
        public void FillRequest(JObject pairs, Dictionary<string, string> globalPairs)
        {
            try
            {
                _requestParams.RemoveAll();
                FillRequest(pairs, _requestFillKeys, globalPairs, ref _requestParams);
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }

        /// <summary>
        /// 填充Json的Body
        /// </summary>
        /// <param name="recvJsonStr">接收到的字符串</param>
        public EScriptRunStatus FillRequest(string recvJsonStr)
        {
            try
            {
                _interpreter.InputParams.Add("json_str", recvJsonStr);
                dynamic result = null;
                var status = _interpreter.Execute(ref result);
                if(status != EScriptRunStatus.OK)
                {
                    $"脚本执行失败!description:{status.GetDescription()}".LogError();
                    return status;
                }
                _requestParams = JObject.Parse(result);

                return EScriptRunStatus.OK;
            }
            catch (Exception ex)
            {
                ex.Log();
                return EScriptRunStatus.Exception;
            }
        }

        /// <summary>
        /// 执行Request动作
        /// </summary>
        /// <param name="response"></param>
        /// <param name="isSaveResult">是否保存上传结果</param>
        /// <param name="saveDir">保存的路径</param>
        /// <returns></returns>
        private UploadHttpStatus Execute(ref IRestResponse response, bool isSaveResult = false, string saveDir = "E:\\UploadTraceDemo\\")
        {
            if (_uri == null)
            {
                $"Uri为null，执行失败!".LogError();
                return UploadHttpStatus.UriError;
            }

            try
            {
                string path = saveDir.FixedDir();
                path += DateTime.Today.ToString("yyyyMMdd");
                path = path.FixedDir();
                path.CreateDir();
                path += $"{DateTime.Now:yyyyMMdd_HHmmss}.txt";

                var client = new RestClient(_uri);
                client.Timeout = Timeout;
                var request = new RestRequest(RequestMethod);
                //从其他步骤中寻找回复的字段的值
                if(UsingBeforeStepIdx >= 0)
                {
                    JObject otherResults = _getOtherStepResult?.Invoke(UsingBeforeStepIdx);
                    if(otherResults != null)
                    {
                        foreach(var it in otherResults)
                        {
                            if (_requestParams.ContainsKey(it.Key))
                            {
                                _requestParams[it.Key] = it.Value;
                            }
                        }
                    }
                }
                //todo: 根据不同的方式 分割RequestParams
                request.AddHeader("Accept", Accept);
                string body = _requestParams.ToString();
                request.AddParameter(ContentType, body, ParameterType.RequestBody);
                if (isSaveResult)
                {
                    using FileStream fs = File.OpenWrite(path);
                    fs.Write(encoding.GetBytes($"{body}{Environment.NewLine}"));
                    response = client.Execute(request);
                    fs.Write(encoding.GetBytes(response.Content));
                }
                else
                {
                    response = client.Execute(request);
                }

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (_needParamsFromResponse != null && _needParamsFromResponse.Count > 0)
                    {
                        JObject responsePairs = JObject.Parse(response.Content);
                        foreach (var it in responsePairs)
                        {
                            if (_needParamsFromResponse.ContainsKey(it.Key))
                            {
                                _needParamsFromResponse[it.Key] = it.Value;
                            }
                        }
                    }
                    $"服务器回复消息:{response.Content}".LogInfo();
                }
                else
                {
                    if (!response.ErrorMessage.IsNullOrEmpty())
                    {
                        $"服务器回复错误信息:{response.ErrorMessage}".LogError();
                    }
                    return UploadHttpStatus.Error;
                }
            }
            catch (Exception ex)
            {
                ex.Log();
                return UploadHttpStatus.Exception;
            }

            return UploadHttpStatus.OK;
        }

        public override GlobalCode Execute(ref string responseMsg, bool isSaveResult = false, string saveDir = "E:\\UploadTraceDemo\\")
        {
            IRestResponse response = null;
            GlobalCode ret = GlobalCode.OK;

            var status = Execute(ref response, isSaveResult, saveDir);

            if(response == null)
            {
                responseMsg = "Request ERROR!";
                return GlobalCode.Exception;
            }

            if(response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                ret = GlobalCode.Error;
                responseMsg += $"Staus Code={response.StatusCode}";
            }
            responseMsg += response.Content;

            return ret;
        }

        /// <summary>
        /// 序列化为xml文件
        /// </summary>
        /// <param name="path"></param>
        public void SerializerXml(string path)
        {
            if (!path.EndsWith(".xml"))
            {
                $"路径:{path}错误!只能序列化为xml文件!".LogError();
                return;
            }

            try
            {
                IVXmlSerializerManager.Serializer(path, this);
                $"已经将类序列化为xml,路径:{path}".LogInfo();
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
    }

    [Serializable]
    public class RecordReportStep : BaseStep
    {
        public RecordReportStep()
        {

        }

        public override GlobalCode Execute(ref string responseMsg, bool isSaveResult = false, string saveDir = "E:\\UploadTraceDemo\\")
        {
            throw new NotImplementedException();
        }

        public override GlobalCode Init()
        {
            throw new NotImplementedException();
        }
    }

    [Serializable]
    public class UploadStep
    {
        public BaseStep Step { get; set; }

        public void SetMethod(UploadMethod method)
        {
            if(method == UploadMethod.Http)
            {
                Step = new UploadHttpStep();
            }
        }
    }

    public class UploadFunc
    {
        //上传的参数模板字典
        private Dictionary<string, UploadStep> _uploadStepDic = new();
        //步骤配置路径
        private readonly string _stepConfigDir = $"{Environment.CurrentDirectory}\\Config\\StepConfig";
        //全局字典的配置加载路径
        private readonly string _globalConfigName = $"{Environment.CurrentDirectory}\\Config\\GlobalConfig.json";
        //全局配置字典
        private Dictionary<string, string> _globalConfigDic = new();
        //接收到的JObject类型的数据
        private JObject _recvJObject = null;
        //接收到的字符串
        private string _recvString = string.Empty;

        /// <summary>
        /// 编码
        /// </summary>
        public Encoding MyEncoding { get; set; } = Encoding.Default;

        public UploadFunc()
        {
            if (!Directory.Exists(_stepConfigDir))
            {
                $"没有找到配置文件根目录：{_stepConfigDir}，故而无法配置上传!".LogError();
                return;
            }

            if (File.Exists(_globalConfigName))
            {
                string contents = File.ReadAllText(_globalConfigName);
                try
                {
                    JObject pairs = JObject.Parse(contents);
                    _globalConfigDic.Clear();
                    foreach(var it in pairs)
                    {
                        if(it.Value.Type == JTokenType.String)
                        {
                            if (_globalConfigDic.ContainsKey(it.Key))
                            {
                                $"检测倒重复键,key={it.Key}".LogWarn();
                                continue;
                            }
                            _globalConfigDic.Add(it.Key, it.Value.ToString());
                        }
                        else
                        {
                            $"内容错误,目前只能加载类型为string的json，key={it.Key}".LogError();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ex.Log();
                }
            }
            else
            {
                $"没有找到全局配置文件:{_globalConfigName},故而不加载全局配置!".LogWarn();
            }

            try
            {
                DirectoryInfo directoryInfo = new(_stepConfigDir);
                var allFiles = directoryInfo.GetFiles("*.xml");
                foreach(var it in allFiles)
                {
                    UploadStep step = (UploadStep)IVXmlSerializerManager.Deserialization(it.FullName, typeof(UploadStep));
                    _uploadStepDic.Add(it.Name, step);
                }
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }

        /// <summary>
        /// 设置接收到的JObject数据,设置JObject之后，接收到的其他类型的数据将被清空
        /// </summary>
        /// <param name="pairs"></param>
        public void SetReceiveData(JObject pairs)
        {
            _recvJObject = pairs;
            _recvString = string.Empty;

        }

        /// <summary>
        /// 设置所接收到的字符串,设置string之后，接收到的其他类型的数据将被清空
        /// </summary>
        /// <param name="recStr"></param>
        public void SetReceiveData(string recStr)
        {
            _recvString = recStr;
            _recvJObject = null;
        }

        /// <summary>
        /// 执行步骤，如果有外部接收的数据，那么调用此函数之前，请先调用SetReceiveData进行设置
        /// </summary>
        /// <returns></returns>
        public string Execute()
        {
            string retMsg = string.Empty;
            for (int i = 0; i < _uploadStepDic.Count; i++)
            {
                _uploadStepDic.ElementAt(i).Value.Step.Init();
                if(_uploadStepDic.ElementAt(i).Value.Step.UploadMethod == UploadMethod.Http)
                {
                    if(_recvJObject != null)
                    {
                        ((UploadHttpStep)_uploadStepDic.ElementAt(i).Value.Step).FillRequest(_recvJObject, _globalConfigDic);
                    }
                    else if (!_recvString.IsNullOrEmpty())
                    {
                        ((UploadHttpStep)_uploadStepDic.ElementAt(i).Value.Step).FillRequest(_recvString);
                    }
                }

                string resMsg = string.Empty;
                _uploadStepDic.ElementAt(i).Value.Step.Execute(ref resMsg, true);
                retMsg += $"step index:{_uploadStepDic.ElementAt(i).Value.Step.StepIndex}, Msg:{resMsg}";
            }

            return retMsg;
        }
    }
}
