﻿using System.Collections.Generic;
using UploadTrace.Core;
using InVision.Interpreter;
using System.IO;
using InVision.Base;
using System;
using InVision.Log;
using UploadTrace.Service.Func;

namespace UploadTrace.Service.UploadWorkers
{
    public class UploadWorker : IUploadWorker
    {
        private readonly PythonInterpreter _interpreter = null;
        private readonly UploadFunc _uploadFunc = new UploadFunc();

        public UploadWorker(string codeOrPath)
        {
            _interpreter = new PythonInterpreter();

        }

        public UploadWorker(string codeOrPath, IDictionary<string, object> options)
        {
            _interpreter = new PythonInterpreter(options);

            if (File.Exists(codeOrPath))
            {
                _interpreter.CodePath = codeOrPath;
            }
            else
            {
                _interpreter.RunCodeStr = codeOrPath;
            }
        }

        public string Upload(string json, Dictionary<string, string> extraInfo)
        {
            try
            {
                _interpreter.InputParams.Add("json_str", json);
                dynamic result = null;
                var status = _interpreter.Execute(ref result);
                if (status != EScriptRunStatus.OK)
                {
                    return status.GetDescription();
                }

                return status.GetDescription();
            }
            catch (Exception ex)
            {
                ex.Log();
                return $"ERROR:上传执行失败!Msg:{ex.Message}";
            }
        }

        public string Name { get; set; }
    }
}