﻿using System.Collections.Generic;
using UploadTrace.Core;

namespace UploadTrace.Service.UploadWorkers
{
    public class DllUploadWorker : IUploadWorker
    {
        private readonly IUploadWorker _impl;

        public string Upload(string json, Dictionary<string, string> extraInfo)
        {
            return _impl.Upload(json, extraInfo);
        }

        public string Name { get; set; }

        public DllUploadWorker(IUploadWorker impl)
        {
            _impl = impl;
        }
    }
}