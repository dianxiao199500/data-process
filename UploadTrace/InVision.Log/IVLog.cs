﻿using System;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "IVSyncLog.config", Watch = true)]
namespace InVision.Log
{
    /// <summary>
    /// 同步日志
    /// </summary>
    public static class IVSyncLog
    {
        public static string GetSupInfo()
        {
            try
            {
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(true);
                var frame = st.FrameCount > 3 ? st.GetFrame(3) : st.GetFrame(0);
                return $"{frame.GetFileName()} | {frame.GetMethod()} | line:{frame.GetFileLineNumber()} | ";
            }
            catch
            {
                return string.Empty;
            }

        }

        public static bool IsWriteToConsole { get; set; } = false;

        private static readonly log4net.ILog logInfo = log4net.LogManager.GetLogger("loginfo");  // 这里的 loginfo 和 log4net.config 里的名字要一样
        private static readonly log4net.ILog logError = log4net.LogManager.GetLogger("logerror");// 这里的 logerror 和 log4net.config 里的名字要一样
        private static readonly log4net.ILog logWarn = log4net.LogManager.GetLogger("logwarn");
        private static readonly log4net.ILog logDebug = log4net.LogManager.GetLogger("logdebug");
        private static readonly log4net.ILog logFatal = log4net.LogManager.GetLogger("logfatal");
        public static event Action<string> LogEvent;

        public static void IV_INFO(object mess)
        {
            if (logInfo.IsInfoEnabled)
            {
                string msg = $"{GetSupInfo()}{mess}";
                logInfo.Info(msg);

                LogEvent?.Invoke($"{DateTime.Now:yyyy/MM/dd HH:mm:ss-fff} | Info | {msg}");
                if (IsWriteToConsole)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"{GetSupInfo()}{mess}");
                }
            }
        }

        public static void IV_DEBUG(object mess, Exception ex)
        {
            if (logDebug.IsDebugEnabled)
            {
                string msg = $"{GetSupInfo()}{mess}";
                LogEvent?.Invoke($"{DateTime.Now:yyyy/MM/dd HH:mm:ss-fff} | Debug | {msg}");

                logDebug.Debug($"{GetSupInfo()}{mess}", ex);
                if (IsWriteToConsole)
                {
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine($"{GetSupInfo()}{mess}");
                }
            }
        }

        public static void IV_DEBUG(object mess)
        {
            if (logDebug.IsDebugEnabled)
            {
                string msg = $"{GetSupInfo()}{mess}";
                LogEvent?.Invoke($"{DateTime.Now:yyyy/MM/dd HH:mm:ss-fff} | Debug | {msg}");

                logDebug.Debug($"{GetSupInfo()}{mess}");
                if (IsWriteToConsole)
                {
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine($"{GetSupInfo()}{mess}");
                }
            }
        }

        public static void IV_WARN(object mess, Exception ex)
        {
            if (logWarn.IsWarnEnabled)
            {
                string msg = $"{GetSupInfo()}{mess}";
                LogEvent?.Invoke($"{DateTime.Now:yyyy/MM/dd HH:mm:ss-fff} | Warn | {msg}");

                logWarn.Warn($"{GetSupInfo()}{mess}", ex);
                if (IsWriteToConsole)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"{GetSupInfo()}{mess}");
                }
            }
        }

        public static void IV_WARN(object mess)
        {
            if (logWarn.IsWarnEnabled)
            {
                string msg = $"{GetSupInfo()}{mess}";
                LogEvent?.Invoke($"{DateTime.Now:yyyy/MM/dd HH:mm:ss-fff} | Warn | {msg}");

                logWarn.Warn($"{GetSupInfo()}{mess}");
                if (IsWriteToConsole)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"{GetSupInfo()}{mess}");
                }
            }
        }

        public static void IV_ERROR(object mess, Exception ex)
        {
            if (logError.IsErrorEnabled)
            {
                string msg = $"{GetSupInfo()}{mess}";
                LogEvent?.Invoke($"{DateTime.Now:yyyy/MM/dd HH:mm:ss-fff} | Error | {msg}");

                logError.Error($"{GetSupInfo()}{mess}", ex);
                if (IsWriteToConsole)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{GetSupInfo()}{mess},Exception:{ex}");
                }
            }
        }

        public static void IV_ERROR(object mess)
        {
            if (logError.IsErrorEnabled)
            {
                string msg = $"{GetSupInfo()}{mess}";
                LogEvent?.Invoke($"{DateTime.Now:yyyy/MM/dd HH:mm:ss-fff} | Error | {msg}");

                logError.Error($"{GetSupInfo()}{mess}");
                if (IsWriteToConsole)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{GetSupInfo()}{mess}");
                }
            }
        }

        public static void IV_FATAL(object mess, Exception ex)
        {
            if (logFatal.IsFatalEnabled)
            {
                string msg = $"{GetSupInfo()}{mess}";
                LogEvent?.Invoke($"{DateTime.Now:yyyy/MM/dd HH:mm:ss-fff} | Fatal | {msg}");

                logFatal.Fatal($"{GetSupInfo()}{mess}", ex);
                if (IsWriteToConsole)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{GetSupInfo()}{mess}Exception:{ex}");
                }
            }
        }

        public static void IV_FATAL(object mess)
        {
            if (logFatal.IsFatalEnabled)
            {
                string msg = $"{GetSupInfo()}{mess}";
                LogEvent?.Invoke($"{DateTime.Now:yyyy/MM/dd HH:mm:ss-fff} | Fatal | {msg}");

                logFatal.Fatal($"{GetSupInfo()}{mess}");
                if (IsWriteToConsole)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{GetSupInfo()}{mess}");
                }
            }
        }

        public static void LogError(this string str)
        {
            IV_ERROR(str);
        }

        public static void LogWarn(this string str)
        {
            IV_WARN(str);
        }

        public static void LogInfo(this string str)
        {
            IV_INFO(str);
        }

        public static void LogDebug(this string str)
        {
            IV_DEBUG(str);
        }

        public static void LogFatal(this string str)
        {
            IV_FATAL(str);
        }

        public static void Log(this Exception ex)
        {
            IV_FATAL(ex);
        }

        public static void Log(this Exception ex, string addMsg)
        {
            IV_FATAL(addMsg, ex);
        }
    }
}
