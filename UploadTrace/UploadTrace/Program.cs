﻿using System;
using System.Collections.Generic;
using System.IO;
using InVision.Log;
using Newtonsoft.Json;
using InVision.Communication;
using System.Text;
using Newtonsoft.Json.Linq;

namespace UploadTrace
{
    public class SingleInfo
    {
        public string Value { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public override string ToString()
        {
            return $"值={Value};描述={Description}";
        }
    }

    class Program
    {
        private static readonly string ProgramConfigPath = $"{Environment.CurrentDirectory}\\Config\\ProgramConfig.ini";

        public static Dictionary<string, SingleInfo> ProgramConfigDic = new();

        public static string KeyOfServerPort { get; } = "ServerPort";

        /// <summary>
        /// 是否使用帧控制解析
        /// </summary>
        public static string KeyOfIsUseFrameControl { get; set; } = "IsUseFrameControl";
        public static bool IsUseFrameControl { get; set; } = true;

        /// <summary>
        /// 超过多少长度不回复Client
        /// </summary>
        public static string KeyOfMaxLenNotResponseClient { get; set; } = "MaxLenNotResponseClient";
        public static int MaxLenNotResponseClient { get; set; } = 1024;

        public static SocketServerController SocketServer { get; set; }

        public static void ExitWaitKey()
        {
            if (SocketServer != null && SocketServer.IsListening)
            {
                SocketServer.StopListen();
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("按下任意键退出...");
            Console.ReadKey();
            Environment.Exit(0);
        }

        static void Main(string[] args)
        {
            IVSyncLog.IsWriteToConsole = true;

            $"从路径{ProgramConfigPath}中读取配置信息...".LogInfo();
            if (!File.Exists(ProgramConfigPath))
            {
                $"文件{ProgramConfigPath}不存在!请确认配置文件后重启软件!".LogError();
                ExitWaitKey();
            }

            try
            {
                var contents = File.ReadAllLines(ProgramConfigPath);
                int idx = 1;
                foreach(var it in contents)
                {
                    if (!it.Contains(":"))
                    {
                        $"检查到第{idx}行配置错误,内容为:{it}".LogWarn();
                        idx++;
                        continue;
                    }

                    var singleParams = it.Split(':');
                    string key = singleParams[0].Trim();
                    if (ProgramConfigDic.ContainsKey(key))
                    {
                        $"检查到第{idx}行，配置名称Name={key}重复！".LogWarn();
                        idx++;
                        continue;
                    }

                    SingleInfo singleInfo = new();
                    if (singleParams[1].Contains("#"))
                    {
                        var tmpStrs = singleParams[1].Split('#');
                        singleInfo.Value = tmpStrs[0].Trim();
                        singleInfo.Description = tmpStrs[1].Trim();
                    }
                    else
                    {
                        singleInfo.Value = singleParams[1].Trim();
                    }

                    ProgramConfigDic.Add(key, singleInfo);
                    idx++;
                }

                if (!ProgramConfigDic.ContainsKey(KeyOfServerPort))
                {
                    $"未找到服务器的端口号参数设置，请添加Name={KeyOfServerPort}的参数设置!".LogError();
                    ExitWaitKey();
                }

                if (ProgramConfigDic.ContainsKey(KeyOfIsUseFrameControl))
                {
                    if(bool.TryParse(ProgramConfigDic[KeyOfIsUseFrameControl].Value, out bool result))
                    {
                        IsUseFrameControl = result;
                    }
                }

                if (ProgramConfigDic.ContainsKey(KeyOfMaxLenNotResponseClient))
                {
                    if (int.TryParse(ProgramConfigDic[KeyOfMaxLenNotResponseClient].Value, out int result))
                    {
                        MaxLenNotResponseClient = result;
                    }
                }

                if (int.TryParse(ProgramConfigDic[KeyOfServerPort].Value, out int port))
                {
                    SocketServer = new SocketServerController(port)
                    {
                        IsUseFrameControl = IsUseFrameControl
                    };
                    SocketServer.ServerError += str => $"服务器错误:{str}".LogError();
                    SocketServer.ServerStart += () => $"本地IP={SocketTool.GetLocalIp()},开始监听端口:{port}".LogInfo();
                    SocketServer.ServerStop += () => $"服务器停止监听端口:{port}".LogInfo();
                    SocketServer.ClientConnect += ipEndPoint => $"ip={ipEndPoint.Address},Port={ipEndPoint.Port},连接成功!".LogInfo();
                    SocketServer.ClientConnectionLost += (ipEndPoint, str) => $"ip={ipEndPoint.Address},Port={ipEndPoint.Port}断开连接!错误信息:{str}".LogWarn();
                    SocketServer.ReceiveByte += (ipEndPoint, bytes) =>
                    {
                        string recStr = Encoding.Default.GetString(bytes);
                        $"接收到{ipEndPoint.Address}的消息:{recStr}".LogDebug();

                        JObject jsonObj = null;
                        try
                        {
                            //jsonObj = JObject.Parse(recStr);
                            //Test.SaveFile($"E:\\{DateTime.Now:yyyyMMddHHmmss}.json", recStr);
                            //Test.SaveKeyValEquallyJson(jsonObj, "E:\\Demo.json", Encoding.Default);
                            //Test.DoUploadOneTest("http://d667ede4-38cb-45c7-a0a4-a9076b9e9495.mock.pstmn.io/hello",
                            //    $"{Environment.CurrentDirectory}\\Config\\UploadConfig\\Step1.json", jsonObj);
                            Test.DoUploadTest(recStr);
                        }
                        catch (Exception ex)
                        {
                            "字符串转json失败!".LogError();
                            ex.Log();
                        }

                        if(bytes.Length <= MaxLenNotResponseClient)
                        {
                            SocketServer.SentToClient(ipEndPoint, bytes);
                        }
                        else
                        {
                            $"接收到的数据长度为:{bytes.Length},大于长度:{MaxLenNotResponseClient},所以不回复Client".LogDebug();
                        }
                    };

                    SocketServer.StartListen();
                }
                else
                {
                    $"无法将Name={KeyOfServerPort},中的Value={ProgramConfigDic[KeyOfServerPort].Value}转换为int".LogError();
                    ExitWaitKey();
                }
            }
            catch (Exception ex)
            {
                ex.Log();
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("按下\"q\"或者\"esc\"退出程序...");
            var rKey = Console.ReadKey();
            while(rKey.Key != ConsoleKey.Q || rKey.Key == ConsoleKey.Escape)
            {
                rKey = Console.ReadKey();
            }
            ExitWaitKey();
        }
    }
}
