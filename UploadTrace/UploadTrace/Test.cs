﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using InVision.Log;
using System.IO;
using InVision.Base;
using UploadTrace.Service.Func;

namespace UploadTrace
{
    public static class Test
    {
        public static void DoUploadOneTest(string uri, string configName, JObject pairs, int stepIndex = 0)
        {
            //UploadStep step = new();
            //step.SetMethod(UploadMethod.Http);
            //((UploadHttpStep)step.Step).Init(uri, stepIndex);
            //((UploadHttpStep)step.Step).LoadKeysFromConfig(configName);
            //((UploadHttpStep)step.Step).FillRequest(pairs, null);

            //string resMsg = string.Empty;
            //var status = step.Step.Execute(ref resMsg);
            //$"执行结果:{status},msg={resMsg}".LogDebug();

            ////JsonSerializer.ToJson(step, "E:\\stepConfig.json");
            //IVXmlSerializerManager.Serializer("E:\\stepConfig.xml", step);
        }

        public static void DoUploadTest(string recDatas)
        {
            UploadFunc func = new UploadFunc();
            func.SetReceiveData(recDatas);
            func.Execute();
        }

        public static void SaveFile(string path, string content)
        {
            try
            {
                using FileStream fs = File.OpenWrite(path);
                fs.Write(Encoding.Default.GetBytes(content));
                $"文件已保存到{path}".LogInfo();
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }

        /// <summary>
        /// 保存key和value相同的json示例，用于创建上传所需字段的key 和上传所需value的字段对应的现有的key的 映射关系
        /// 即 <上传的key, 现有的key>
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="path"></param>
        public static void SaveKeyValEquallyJson(JObject obj, string path, Encoding encoding)
        {
            try
            {
                static void FillObj(ref JObject pairs)
                {
                    foreach (var it in pairs)
                    {
                        if (it.Value.Type == JTokenType.Object || it.Value.Type == JTokenType.Array)
                        {
                            JObject tmpObj = it.Value.ToObject<JObject>();
                            FillObj(ref tmpObj);
                            pairs[it.Key] = tmpObj;
                            continue;
                        }

                        pairs[it.Key] = it.Key;
                    }
                }

                FillObj(ref obj);
                using FileStream fs = File.OpenWrite(path);
                fs.Write(encoding.GetBytes(obj.ToString()));
                $"文件已保存到{path}".LogInfo();
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
    }
}
